
import XCTest
@testable import KCiOS

class KCiOSTests: XCTestCase
{
    
    func testProducts()
    {
    
    }
    
    
    func testLogin()
    {
        let expectation = expectationWithDescription("products")

        OperationFactory().loginOperation(LoginParameters(userName: "userApi", password: "000")).onSuccess { (JSONResponse) in
            print(JSONResponse)
            expectation.fulfill()
        }.onFailure { error in
            expectation.fulfill()
            XCTFail("\(error)")
        }
        
        waitForExpectationsWithTimeout(10, handler: { error in
            if let error = error {
                XCTFail("waitForExpectationsWithTimeout errored: \(error)")
            }
        })
    }
    
    func testRegisterUser(){
        
        let expectation = expectationWithDescription("register")

        let registerParameters = RegisterParameters(userName: "test1", password:"000", email:  "prueba@prueba.com", name:"noeke")
        OperationFactory().registerUserOperation(registerParameters).onSuccess { (JSON) in
            
            print(JSON)
            expectation.fulfill()
            }.onFailure { error in
                XCTFail("\(error)")
                expectation.fulfill()
        }
        
        waitForExpectationsWithTimeout(10, handler: { error in
            if let error = error {
                XCTFail("waitForExpectationsWithTimeout errored: \(error)")
            }
        })
    }
    
    func testRegisterParameters() {
        
        //let registerParameters = RegisterParameters(userName: "test1", password:"000", email:  "prueba@prueba.com", name:"noeke").registerModelToDictionary()
        
    }
    
}
