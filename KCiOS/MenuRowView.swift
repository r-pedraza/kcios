
import UIKit

class MenuRowView: UIView
{
    @IBOutlet weak var iconView: UIImageView!
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var separator: UIView!
    
    static func createMenuView() -> MenuRowView
    {
        let bundle = NSBundle.mainBundle()
        let nib = UINib(nibName: String(self), bundle: bundle)
        let root = nib.instantiateWithOwner(self, options: nil)[0] as! MenuRowView
        root.separator.backgroundColor = UIColor.blackColor()
        return root
    }
}