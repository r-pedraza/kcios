//
//  NSData+Extension.swift
//  KCiOS
//
//  Created by Raúl Pedraza on 12/7/16.
//  Copyright © 2016 Raúl Pedraza. All rights reserved.
//


import Foundation


extension NSData
{
    public func toJSON() -> Dictionary<String,AnyObject>?
    {
        var json: AnyObject?
        do {
            json = try NSJSONSerialization.JSONObjectWithData(self, options: NSJSONReadingOptions.AllowFragments)
            //dlog(String(data: self, encoding: NSUTF8StringEncoding), .Trace)
            if let arrayJson = json {
                if arrayJson.isKindOfClass(NSArray){
                    return ["response":arrayJson]
                }
            }
            
            guard let jsonDic = json as? Dictionary<String,AnyObject> else {
                return nil
            }
            return jsonDic
        } catch {
            var asString = NSString(data:self, encoding:NSUTF8StringEncoding)
            if let string = asString {
                asString = string
            }
            dlog("Couldn't parse as JSON. Data was: bytes=\(self.length), interpretation as UTF8:\n\(asString)", .Warn)
        }
        return nil
    }
    
    public var UTF8String: String? {
        get {
            return NSString(data:self, encoding:NSUTF8StringEncoding) as? String
        }
    }
    
}