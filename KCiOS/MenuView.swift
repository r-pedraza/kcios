
import UIKit


class TableTopView: UIView { }


final class MenuView: UIView, UITableViewDelegate
{
    private var tableTopView: TableTopView = TableTopView()
    private var tableView: UITableView = UITableView()
    private var dataSource: TableDataSource?
    private var delegate: UITableViewDelegate?
    private var data: Array<MenuCellModel> = []
    
    private var cellHeight: CGFloat = 0.0
    
    var handleClick: (model: MenuCellModel)->() = { model in }
    
    
    // MARK:- Data
    
    func setRows(data: Array<MenuCellModel>)
    {
        dispatch_async(dispatch_get_main_queue()) {
            self.data = data
            
            self.dataSource = TableDataSource(data: data, tableView: self.tableView, cell: MenuViewCell.self)
            self.delegate = self
            
            self.tableView.dataSource = self.dataSource
            self.tableView.delegate = self.delegate
            
            dlog("IM RELOADING THE DATA \(data)", .Warn)
            self.tableView.reloadData()
        }
    }
    
    
    // MARK:- Initialization
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setupView()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    
    // MARK:- Setup
    
    private func setupView()
    {
        // build
        addSubview(tableTopView)
        addSubview(tableView)

        // customize
        cellHeight = MenuRowView.createMenuView().frame.size.height
        
        // layout
        let views = ["tableView": tableView, "tableTopView": tableTopView]
        views.each { (_, view) -> () in
            view.translatesAutoresizingMaskIntoConstraints = false
        }
        for constraint in [
            "V:|[tableTopView(40)][tableView]|",
            "H:|[tableTopView]|",
            "H:|[tableView]|"
            ] {
                addConstraints(NSLayoutConstraint.constraintsWithVisualFormat(constraint, options: [], metrics: nil, views: views))
        }
    }
    
    // MARK:- UITableViewDelegate
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        handleClick(model: data[indexPath.row])
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return cellHeight
    }
    
}
