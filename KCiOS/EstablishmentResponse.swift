//
//  EstablishmentResponse.swift
//  KCiOS
//
//  Created by Raúl Pedraza on 6/9/16.
//  Copyright © 2016 Raúl Pedraza. All rights reserved.
//


import Foundation

/** Response from a login operation. */
public struct EstablishmentResponse: JSONResponse{
    
    var establishment = Array<Establishment>()
    
    public static func createInstance(json: ResponseDictionary) -> EstablishmentResponse?
    {
        var establishmentResponse = EstablishmentResponse()
        if let data = json["data"] as? Array<Dictionary<String,AnyObject>> {
            
            establishmentResponse.establishment = data.map({ dic -> Establishment in
                return Establishment(dictionary: dic)
            })
        }
        
        return establishmentResponse
    }
}
