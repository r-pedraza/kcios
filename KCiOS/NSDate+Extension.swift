//
//  NSDate+Extension.swift
//  KCiOS
//
//  Created by Raúl Pedraza on 13/7/16.
//  Copyright © 2016 Raúl Pedraza. All rights reserved.
//

import Foundation

extension NSDate{
    
   public func dateStringToDateFormatt(dateString:String) -> NSDate {
        let dateFor: NSDateFormatter = NSDateFormatter()
        dateFor.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        let yourDate: NSDate? = dateFor.dateFromString(dateString)
        return yourDate!
    }
}