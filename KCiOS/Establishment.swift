//
//  Establishment.swift
//  KCiOS
//
//  Created by Raúl Pedraza on 5/9/16.
//  Copyright © 2016 Raúl Pedraza. All rights reserved.
//

import UIKit


class Establishment {
    var imageType:UIImage?
    var imageMain:UIImage?
    var comments: String?
    var latitude: String?
    var longitude:String?
    var name:     String?
    var address:  String?
    var distance:  String?


    
    init(dictionary:Dictionary<String,AnyObject>) {
        self.name      = dictionary["name"]    as? String
        self.address   = dictionary["address"] as? String
        self.latitude  = dictionary["latitude"] as? String
        self.longitude = dictionary["longitude"] as? String
//        self.imageType = dic
//        self.imageMain = imageMain
//        self.comments  = comments
//        self.distance  = distance
    }
}