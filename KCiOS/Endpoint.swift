
import Foundation
import Alamofire

/** 
 Kind of authentication needed by an endpoint.
 All operations need either "Authorization: Basic SECRET", or "AppId: SESSION".
 */
enum APIAuthentication
{
    /** Doesn't need any authorization */
    case Public
    /** Needs Authorization Basic with the app secret. */
    case Authorization
    /** Needs an 'appId' header with the session created by the login operation. */
    case AppIdentifier
}

/** Representation of an API endpoint. */
struct APIEndpoint
{
    let method: Alamofire.Method
    let encoding: ParameterEncoding
    let path: String
    let parameters: Dictionary<String,AnyObject>?
    let authentication: APIAuthentication
    let responseType: JSONResponse.Type
    let body: NSData?

    init(_ method: Alamofire.Method,
         _ encoding: ParameterEncoding,
         _ path: String,
         _ parameters: Dictionary<String,AnyObject>?,
         _ authentication: APIAuthentication,
         _ responseType: JSONResponse.Type,
         _ body: NSData) 
    {
        self.method = method
        self.encoding = encoding
        self.path = path
        self.parameters = parameters
        self.authentication = authentication
        self.responseType = responseType
        self.body = body
    }
    
}