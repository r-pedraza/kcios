
import UIKit

class MenuViewCell: UITableViewCell, ConfigurableCell
{
    var menuRowView: MenuRowView!
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?)
    {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        menuRowView = MenuRowView.createMenuView()
        addSubview(menuRowView)
        
        menuRowView.translatesAutoresizingMaskIntoConstraints = false
        for constraint in ["V:|[menuRowView]|", "H:|[menuRowView]|"] {
            addConstraints(NSLayoutConstraint.constraintsWithVisualFormat(constraint, options: [], metrics: nil, views: ["menuRowView":menuRowView]))
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func configureWithObject<T>(object: T) -> Void
    {
        let model = object as? MenuCellModel
        menuRowView.iconView.image = model?.iconNormal
        menuRowView.title.text = model?.title
        
        self.hidden = (model?.state == .Hidden)
    }

}