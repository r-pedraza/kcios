//
//  RegisterFormCell.swift
//  KCiOS
//
//  Created by Raúl Pedraza on 9/7/16.
//  Copyright © 2016 Raúl Pedraza. All rights reserved.
//

import UIKit

class RegisterFormCell: UITableViewCell {

    @IBOutlet weak var texfieldCell: UITextField!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setupBottomBorder()
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func identifierCell() -> String {
        return "CELL";
    }
    
    func cellHeigth() -> CGFloat {
        return 50.0;
    }
    
    //MARK:Personalization
    func setupBottomBorder() {
        let border = CALayer()
        let width = CGFloat(1.0)
        border.borderColor = UIColor.darkGrayColor().CGColor
        border.frame = CGRect(x: 0, y: texfieldCell.frame.size.height - width, width:  UIScreen.mainScreen().bounds.size.width, height: 0.5)
        
        border.borderWidth = width
        texfieldCell.layer.addSublayer(border)
        texfieldCell.layer.masksToBounds = true
    }
}
