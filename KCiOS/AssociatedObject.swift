
import Foundation

func associatedObject<ValueType: AnyObject>(base: AnyObject, key: UnsafePointer<UInt8>, initialiser: () -> ValueType) -> ValueType
{
    if let associated = objc_getAssociatedObject(base, key) as? ValueType {
        return associated
    }
    let associated = initialiser()
    objc_setAssociatedObject(base, key, associated, .OBJC_ASSOCIATION_RETAIN)
    return associated
}


func associateObject<ValueType: AnyObject>(base: AnyObject, key: UnsafePointer<UInt8>, value: ValueType)
{
    objc_setAssociatedObject(base, key, value, .OBJC_ASSOCIATION_RETAIN)
}


/* Usage:

// create a key for the associated object
private var sliderKey: UInt8 = 0

extension UINavigationController {
    var slider: Slider {
        get {
            return associatedObject(self, key: &sliderKey){
                assert(false, "The slider wasn't previously set")
                return Slider(slidingViewBlock: { return UIView() } )
            }
        }
        set { associateObject(self, key: &sliderKey, value: newValue) }
    }
}
 
*/
