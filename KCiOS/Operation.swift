

import Foundation
import Alamofire

/** Results from an operation execution.*/
enum OperationResult
{
    case Success(JSONResponse)
    case Failure(OperationError)
    case FailureAfterLogin(OperationError)
}

/**
 Executes API calls.
 */
public class Operation
{
    private let manager        : Alamofire.Manager
    private let router         : Router
    private var operationResult: OperationResult? = nil
    
    
    private static func loginRouter() -> Router?
    {
        if let loginParameters = RouterKeychain.Credentials.read() {
            return Router.Login(p: loginParameters)
        }
        return nil
    }
    
    /**
     Initializer.
     
     - parameter manager: Network configuration.
     - parameter router:  A request
     */
    public init(manager: Alamofire.Manager, router: Router)
    {
        self.manager = manager
        self.router = router
    }
    
    /**
     Find an error in the given response parameters.
     The resulting userFriendlyMessage field can be shown to the user.
     */
    private func extractError(
        request       : Alamofire.Request,
        responseType  : JSONResponse.Type,
        response      : NSHTTPURLResponse?,
        data: NSData?,
        error: NSError?) -> OperationError
    {
//        if let msg = data?.UTF8String, let status = response?.statusCode {
//            dlog("Status code: \(status). String: \(msg)", .Warn)
//        }
        
        var operationError: OperationError
        
        // Check for not connected. Default to unexpected error if it is a different cause.
        guard let statusCode = response?.statusCode else {
            operationError = OperationError(.UnexpectedError)
            if error != nil && error?.code != nil {
                if error?.code == NSURLErrorNotConnectedToInternet {
                    operationError = OperationError(.NotConnectedToTheInternet)
                } else if error?.code == NSURLErrorTimedOut {
                    operationError = OperationError(.TimedOut)
                } else if error?.code == NSURLErrorNetworkConnectionLost {
                    operationError = OperationError(.NetworkConnectionLost)
                }
            }
            return operationError
        }
        
        // check for HTTP error
        guard 200...299 ~= statusCode else {
            let cause: OperationErrorCause
            switch statusCode {
            case 401:       cause = OperationErrorCause.HTTPClientError(statusCode); dlog("\n\(request.debugDescription)")
            case 400...499: cause = OperationErrorCause.HTTPClientError(statusCode); dlog("\n\(request.debugDescription)")
            case 500...599: cause = OperationErrorCause.HTTPServerError(statusCode); dlog("\n\(request.debugDescription)")
            default:        cause = OperationErrorCause.UnexpectedError;             dlog("\n\(request.debugDescription)")
            }
            
            let dic = data?.toJSON()
            var serverMessage: String = cause.description
            
            if let dicResponse = dic {
                if let msg = dicResponse["response"]!["error"]!!["message"] as? String {
                    serverMessage = msg
                } else if let response = dicResponse["response"] as? NSArray {
                    if let msgDic = response.firstObject as? NSDictionary {
                        if let msg = msgDic["message"] as? String {
                            serverMessage = msg
                        }
                    }
                }
            }
            
            return OperationError(userFriendlyMessage: serverMessage, cause: cause)
        }
        
        // check for malformed response
        guard let jsonDic = data?.toJSON(), _ = responseType.createInstance(jsonDic) else {
            return OperationError(.MalformedResponse)
        }
        
        return OperationError(.UnexpectedError)
    }
    
    
    private func run(router: Router) -> OperationResult
    {
        let start                   = CFAbsoluteTimeGetCurrent()

        let request                 = manager.request(router)
        let responseType            = router.endpoint.responseType
        let semaphore               = dispatch_semaphore_create(0)
        var result: OperationResult = .Failure(OperationError(.TimedOut))

        let queue                   = dispatch_get_global_queue(QOS_CLASS_BACKGROUND, 0)
        
        request.response(queue: queue) {
            theRequest, response, data, error in
            
            // happy path to success
            if let statusCode = response?.statusCode,
                let jsonDic = data?.toJSON(),
                let jsonResponse = responseType.createInstance(jsonDic)
                where 200...299 ~= statusCode
            {
                result = .Success(jsonResponse)
                dlog("finished in \(CFAbsoluteTimeGetCurrent() - start) ms\n")
                dispatch_semaphore_signal(semaphore)
                return
            }
            
            result = .Failure(self.extractError(request, responseType: responseType, response: response, data: data, error: error))
            dlog("finished in \(CFAbsoluteTimeGetCurrent() - start) ms\n")
            dispatch_semaphore_signal(semaphore)
        }
        
        dispatch_semaphore_wait(semaphore, DISPATCH_TIME_FOREVER)
        
        return result
    }
    
    
    /**
     Runs the given block on the operation result if such result was a success.
     If there is no result, it runs the operation first.
     */
    public func onSuccess(success:(JSONResponse)->()) -> Operation
    {
        if operationResult == nil {
            operationResult = run(router)
        }
        if case OperationResult.Success(let jsonResponse) = operationResult! {
            success(jsonResponse)
        }
        return self
    }
    
    /**
     Runs the given block on the operation result if such result was a failure.
     If there is no result, it runs the operation first.
     */
    public func onFailure(failure:(OperationError)->()) -> Operation
    {
        if operationResult == nil {
            operationResult = run(router)
        }
        guard let operationResult = operationResult else {
            dlog("The operation lacks a result. This is unexpected.", .Error)
            return self
        }
        switch operationResult {
        case .Failure(let error): failure(error)
        case .FailureAfterLogin(let error): failure(error)
        case .Success: ()
        }
        return self
    }
    
}