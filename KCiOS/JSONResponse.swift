//
//  JSONResponse.swift
//  KCiOS
//
//  Created by Raúl Pedraza on 11/7/16.
//  Copyright © 2016 Raúl Pedraza. All rights reserved.
//


import Foundation

public typealias ResponseDictionary = Dictionary<String,AnyObject>

/** Operation response. */
public protocol JSONResponse: CustomStringConvertible
{
    /** Create an instance of this object with the given dictionary. */
    static func createInstance(json: ResponseDictionary) -> Self?
}

extension JSONResponse
{
    public var description: String {
        return ReflectionUtil.dictionaryForAny(self).description
    }
    
}
