//
//  LoginVC.swift
//  KCiOS
//
//  Created by Raúl Pedraza on 1/10/16.
//  Copyright © 2016 Raúl Pedraza. All rights reserved.
//

import UIKit

class LoginVC: UIViewController {

    @IBOutlet weak var passwordTxf: UITextField!
    @IBOutlet weak var emailTxf: UITextField!
    @IBOutlet weak var userNameTxf: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
    navigationController!.navigationBar.tintColor = UIColor.whiteColor();
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func registerAction(sender: AnyObject) {
    }

    @IBAction func loginAction(sender: AnyObject) {
        self.emailTxf.hidden = true
    }

    @IBAction func recoverPasswordtAction(sender: AnyObject) {
         self.emailTxf.hidden = false
        self.passwordTxf.hidden = true
        self.userNameTxf.hidden = true
    }

}
