//
//  FactoryCell.swift
//  KCiOS
//
//  Created by Raúl Pedraza on 9/7/16.
//  Copyright © 2016 Raúl Pedraza. All rights reserved.
//

import UIKit
import PreviewTransition
enum TitleNibCell:String {
    case RegisterNibCell            = "RegisterFormCell"
    case ProductNibCell             = "ProductCell"
    case CategoryNibCell            = "CategoryCell"
    case EstablishmentNibCell       = "EstablishmentCell"
    case EstablishmentFilterNibCell = "EstablishmentFilterCell"

}

class FactoryCell: NSObject {
    
    func cellWithObject(model:AnyObject, tableView:UITableView?) -> AnyObject {
        
        var cell             :RegisterFormCell?
        var viewModel        :FormField?
        var establishmentCell:EstablishmentCell?


        if (model.isKindOfClass(FormField)) {
            viewModel = model as? FormField;
                cell = tableView!.dequeueReusableCellWithIdentifier(RegisterFormCell().identifierCell()) as? RegisterFormCell
                cell?.texfieldCell.text = viewModel?.placeholder
                cell?.backgroundColor = UIColor.clearColor()
                
                cell?.texfieldCell.rac_textSignal()
                    .takeUntil(cell!.rac_prepareForReuseSignal) // Ignore after cell gets reused
                    .distinctUntilChanged() // Only take value if different from previous one
                    .skip(1) // Skip cell dequeue event
                    .subscribeNext { _ in
                         viewModel!.value = cell?.texfieldCell.text
                }
            return cell!
            }
        

    
        
        if (model.isKindOfClass(Establishment)) {
            
          let model = model as? Establishment
                 establishmentCell = tableView!.dequeueReusableCellWithIdentifier("EstablishmentCell") as? EstablishmentCell
            
            if let name = model?.name , let address = model?.address {
                establishmentCell?.commentsLabel.text =  String(format: "%@ - %@",name,address)
            }

            establishmentCell!.mainEstablishmentImage.image = model?.imageMain
            establishmentCell?.distanceLabel.text = model?.distance
        }
        
        return establishmentCell!
    }
}

