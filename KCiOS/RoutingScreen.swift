//
//  RoutingScreen.swift
//  KCiOS
//
//  Created by Raúl Pedraza on 25/6/16.
//  Copyright © 2016 Raúl Pedraza. All rights reserved.
//

import UIKit
import PreviewTransition
enum StoryBoardName:String{

    case Products      = "Products"
    case Login         = "LoginOrRegister"
    case Register      = "Register"
    case AddProduct    = "AddProduct"
    case ProductDetail = "ProductDetail"
    case Categories    = "Categories"
    case EstablishmentDetail = "EstablishmentDetail"

}

class RoutingScreen: NSObject {
  
    func routingToProducts(navigation:UINavigationController) {
        let storyboard = UIStoryboard(name: StoryBoardName.Products.rawValue, bundle: nil)
        let vc         = storyboard.instantiateViewControllerWithIdentifier("Products")
        navigation.pushViewController(vc, animated: true)
    }

    func routingToRegister(navigation:UINavigationController) {
        let storyboard = UIStoryboard(name: StoryBoardName.Register.rawValue, bundle: nil)
        let vc         = storyboard.instantiateViewControllerWithIdentifier("Register")
        navigation.pushViewController(vc, animated: true)
    }
    
    
    func routingToAddProduct(navigation:UINavigationController) {
        let storyboard = UIStoryboard(name: StoryBoardName.AddProduct.rawValue, bundle: nil)
        let vc         = storyboard.instantiateViewControllerWithIdentifier("AddProduct")
        navigation.pushViewController(vc, animated: true)
    }
    
    func routingToLogin(navigation:UINavigationController) {
        let storyboard = UIStoryboard(name: StoryBoardName.Login.rawValue, bundle: nil)
        let vc         = storyboard.instantiateViewControllerWithIdentifier("Login")
        navigation.pushViewController(vc, animated: true)
    }

    
    func routingToEstablishmentDetail(establishment:Establishment,navigation:UINavigationController) {
        let storyboard = UIStoryboard(name: StoryBoardName.EstablishmentDetail.rawValue, bundle: nil)
        let vc         = storyboard.instantiateViewControllerWithIdentifier("EstablishmentDetail") as? EstablishmentDetailVC
        vc?.establishment = establishment
        vc?.tag = 0
        navigation.pushViewController(vc!, animated: true)
    }
    
    func routingToEstablishmentDetailWithMapView(establishment:Establishment,navigation:UINavigationController) {
        let storyboard = UIStoryboard(name: StoryBoardName.EstablishmentDetail.rawValue, bundle: nil)
        let vc         = storyboard.instantiateViewControllerWithIdentifier("EstablishmentDetail") as? EstablishmentDetailVC
        vc?.establishment = establishment
        vc?.tag = 2
        navigation.pushViewController(vc!, animated: true)
    }
    
    
    func routingTo(storyboard:String,controller:String,navigation:UINavigationController) {
        let storyboard = UIStoryboard(name: storyboard, bundle: nil)
        let vc         = storyboard.instantiateViewControllerWithIdentifier(controller)
        navigation.pushViewController(vc, animated: true)
    }
}
