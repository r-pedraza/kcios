//
//  UserRegisterModel.swift
//  KCiOS
//
//  Created by Raúl Pedraza on 24/9/16.
//  Copyright © 2016 Raúl Pedraza. All rights reserved.
//

import Foundation

class UserRegisterModel {
    var name:String?
    var firstSurname:String?
    var secondSurname:String?
    var email:String?
    var userName:String?
    var password:String?
    var city:String?
    var state:String?
    
}