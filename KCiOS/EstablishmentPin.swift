//
//  EstablishmentPin.swift
//  KCiOS
//
//  Created by Raúl Pedraza on 29/9/16.
//  Copyright © 2016 Raúl Pedraza. All rights reserved.
//

import Foundation
import MapKit
import UIKit
import CoreLocation

class EstablishmentPin: NSObject, MKAnnotation {
    var myCoordinate: CLLocationCoordinate2D
    var title: String?
    var subtitle: String?

    init(myCoordinate: CLLocationCoordinate2D,title:String,subtitle:String) {
        self.myCoordinate = myCoordinate
    }
    
    var coordinate: CLLocationCoordinate2D {
        return myCoordinate
    }
}