
import UIKit

/** Side the slider lays in. */
enum SlidePosition {
    case Left
    case Right
}

/** How far to slide. */
enum SlideLength
{
    case WidthPercent(perOne: CGFloat)
    case WidthPoints(points: CGFloat)
    
    /** Return the length of the slide. */
    func points() -> CGFloat
    {
        let width = UIScreen.mainScreen().bounds.size.width
        switch self {
            case .WidthPercent(let perOne): return width * perOne
            case .WidthPoints(let points):  return points
        }
    }
}


public class Slider: NSObject,MenuActionProtocol
{
    var isEnabled: Bool = true {
        willSet(newValue) {
            if !newValue {
                slideView(slidingViewBlock(), toPosition: .Left, withSpeed: pointsPerSecond)
            }
        }
    }
    
    
    /** Origin of the drag gesture. Updated when the gesture starts. */
    private var dragGestureOriginX: CGFloat = 0.0

    /** View to shift by the drag gesture. */
    private let slidingViewBlock:()->UIView
    
    /** How far to slide the view. */
    private let slideLength: SlideLength
    
    
    /** Speed of the sliding animation. */
    private var pointsPerSecond: CGFloat {
        return UIScreen.mainScreen().bounds.size.width
    }
    
    
    /** @param slidingView View to shift by the drag gesture. */
    convenience init(slidingViewBlock: ()->UIView)
    {
        let thirtyPercent = CGFloat(0.8)
        self.init(slidingViewBlock: slidingViewBlock, slideLength: SlideLength.WidthPercent(perOne: thirtyPercent))
    }
    
    
    /** 
     @param slidingView View to shift by the drag gesture.
     @param slideLength How far to slide the view.
     */
    init(slidingViewBlock: ()->UIView, slideLength: SlideLength)
    {
        self.slidingViewBlock = slidingViewBlock
        self.slideLength = slideLength
        
        super.init()
        
        // drag gesture recognizer
        let recognizer = UIPanGestureRecognizer(target: self, action: #selector(Slider.handleDragGesture))
        recognizer.minimumNumberOfTouches = 1
        recognizer.maximumNumberOfTouches = 1
        slidingViewBlock().addGestureRecognizer(recognizer)
        
        // nav controller shadow
        let layer = slidingViewBlock().layer;
        layer.shadowOpacity = 1.0
        layer.shadowRadius = 2.0
        layer.masksToBounds = false
        layer.shadowColor = UIColor.blackColor().CGColor
        // FPS sauce
        layer.shadowPath = UIBezierPath(rect: slidingViewBlock().bounds).CGPath
        layer.rasterizationScale = UIScreen.mainScreen().scale
        layer.shouldRasterize = false;
    }
    
    
    /** Shift the view along with the finger. */
    public func handleDragGesture(recognizer: UIPanGestureRecognizer)
    {
        guard isEnabled else {
            return
        }
        
        switch (recognizer.state)
        {
            // Finger landed. Save the initial position.
            case .Began:
                dragGestureOriginX = slidingViewBlock().frame.origin.x
                
            // Finger is being dragged. Shift the view along with the finger.
            case .Changed:
                // new x coord is the origin + the distance to our fingertip
                let translation = recognizer.translationInView(slidingViewBlock())
                let newX = dragGestureOriginX + translation.x
                
                // update only if we are between 0 and kSlideWidth
                let screenWidth = UIScreen.mainScreen().bounds.size.width
                let threshold = screenWidth - slideLength.points()
                if ( (newX > 0) && (newX < threshold) )
                {
                    let f = slidingViewBlock().frame
                    let rect = CGRectMake(newX, f.origin.y, f.size.width, f.size.height)
                    slidingViewBlock().frame = rect;
                }
                
            // Finger lifted. Slide the controller to the nearest border.
            case .Ended:
                let translation = recognizer.translationInView(slidingViewBlock())
                let velocity = recognizer.velocityInView(slidingViewBlock())
                let inertia = velocity.x * 0.1; // that's points per second * .1 seconds
                let finalX = dragGestureOriginX + translation.x + inertia;
                
                let nearestBorder: SlidePosition = finalX > (slidingViewBlock().frame.size.width / 2) ? .Right : .Left
                slideView(slidingViewBlock(), toPosition: nearestBorder, withSpeed: velocity.x)
                
            default: ()
        }
    }
    
    
    // animate the navigation controller sideways
    func slideView(view: UIView, toPosition position: SlidePosition, withSpeed velocity:CGFloat)
    {
        // use a minimum of kVelocity
        let velocity = velocity < pointsPerSecond ? pointsPerSecond : velocity
        
        let startX = slidingViewBlock().frame.origin.x
        let endX = position == .Right ? slideLength.points() : 0
        
        // time to slide from 'startX' to 'endX' with the given velocity
        let time = NSTimeInterval(fabs((startX - endX) / velocity))
        
        let f = slidingViewBlock().frame
        let newRect = CGRectMake(endX, f.origin.y, f.size.width, f.size.height)
        UIView.animateWithDuration(time, delay: 0, options: .CurveEaseIn, animations: { () -> Void in
            view.frame = newRect
            }) { (Bool) -> Void in }
    }
    
    
    func togglePosition()
    {
        guard isEnabled else {
            return
        }
        let inverse: SlidePosition = (currentPosition() == .Left) ? .Right : .Left
        slideView(slidingViewBlock(), toPosition: inverse, withSpeed: pointsPerSecond)
    }
    
    
    // tell whether the controller is partially or fully visible
    func currentPosition() -> SlidePosition {
        return (slidingViewBlock().frame.origin.x == 0) ? .Left : .Right;
    }
    
}
