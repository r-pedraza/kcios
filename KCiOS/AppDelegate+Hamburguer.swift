
import UIKit

var sliderKey = UInt8(1)
var menuKey = UInt8(2)


extension AppDelegate
{
    
    var slider: Slider {
        get {
            return associatedObject(self, key: &sliderKey){
                assert(false, "The slider wasn't previously set")
                return Slider(slidingViewBlock: { return UIView() } )
            }
        }
        set { associateObject(self, key: &sliderKey, value: newValue) }
    }
    
    var menuVC: MenuVC {
        get {
            return associatedObject(self, key: &menuKey){
                assert(false, "The slider wasn't previously set")
                return MenuVC()
            }
        }
        set { associateObject(self, key: &menuKey, value: newValue) }
    }
    
//    private func installSliderRootViewController()
//    {
//        let initialVC = InitialVC()
//        initialVC.view.backgroundColor = UIColor.orangeColor()
//        
//        let navigationController = UINavigationController()
//        navigationController.pushViewController(initialVC, animated: false)
//        slider = Slider(slidingViewBlock: { return navigationController.view })
//        window?.rootViewController = navigationController
//        window?.makeKeyAndVisible()
//        
//        // next line must be AFTER makeKeyAndVisible()
//        navigationController.view.superview?.insertSubview(menuVC.view, belowSubview: navigationController.view)
//    }
}

/*
 
// press to slide the navigation controller
navigationItem.leftBarButtonItem = UIBarButtonItem(
    image: UIImage(named:"menu-icon.png"), // icon stolen from the facebook app
    style: .Plain,
    target: self,
    action: #selector(LoginVC.handleNavigationButtonLeft))
}


func handleNavigationButtonLeft()
{
    print("button left")
    navigationController?.slider.togglePosition()
}

*/