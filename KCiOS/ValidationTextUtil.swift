//
//  ValidationTextUtil.swift
//  KCiOS
//
//  Created by Raúl Pedraza on 9/7/16.
//  Copyright © 2016 Raúl Pedraza. All rights reserved.
//

import UIKit
import ReactiveCocoa
class ValidationTextUtil: NSObject {
    
    func validateName(nameTxf:UITextField) -> AnyObject  {
        let validateNameSignal = nameTxf.rac_newTextChannel().map {
            (text:AnyObject!) -> AnyObject! in
            return self.isValidName((text as? String)!) ?? false
        }
        
        return validateNameSignal
    }
    
    
    func isValidName(name:String)->Bool{
    
        if name.characters.count > 0 {
            return true
        }else{
            return false;
        }
    }

}
