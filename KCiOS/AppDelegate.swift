			//
//  AppDelegate.swift
//  KCiOS
//
//  Created by Raúl Pedraza on 19/6/16.
//  Copyright © 2016 Raúl Pedraza. All rights reserved.
//	

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var storyboard:UIStoryboard? = UIStoryboard(name: "Establishments", bundle: nil)

    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?)   -> Bool {

        menuVC                     = storyboard?.instantiateViewControllerWithIdentifier("Menu") as! MenuVC
        let initialVC              = storyboard?.instantiateViewControllerWithIdentifier("Establishments") as! EstablishmentsTableVC
        let navigationController   = UINavigationController(rootViewController: initialVC)
        navigationController.navigationBar.barTintColor = UIColor(red: 0.13, green: 0.32, blue: 0.55, alpha: 1.00)

        slider = Slider(slidingViewBlock: { return navigationController.view })

        window = UIWindow()
        window?.rootViewController = navigationController
        
        window?.makeKeyAndVisible()
        
        // Keep next line BELOW makeKeyAndVisible():
        navigationController.view.superview?.insertSubview(menuVC.view, belowSubview: navigationController.view)
        return true
    }

    func applicationWillResignActive(application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    func action(sender:Any) {
        
    }


}

