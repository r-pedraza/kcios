//
//  EstablishmentCell+Extension.swift
//  KCiOS
//
//  Created by Raúl Pedraza on 8/9/16.
//  Copyright © 2016 Raúl Pedraza. All rights reserved.
//

import Foundation
import UIKit
extension EstablishmentCell : UICollectionViewDelegateFlowLayout {
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        let itemsPerRow:CGFloat = 4
        let hardCodedPadding:CGFloat = 5
        let itemWidth = (collectionView.bounds.width / itemsPerRow) - hardCodedPadding
        let itemHeight = collectionView.bounds.height - (2 * hardCodedPadding)
        return CGSize(width: itemWidth, height: itemHeight)
    }
}