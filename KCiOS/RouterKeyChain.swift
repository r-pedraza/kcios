//
//  RouterKeyChain.swift
//  KCiOS
//
//  Created by Raúl Pedraza on 12/7/16.
//  Copyright © 2016 Raúl Pedraza. All rights reserved.
//


import Foundation


public struct RouterKeychain
{
    public static func isUserLogged() -> Bool {
        return Session.read() != nil
    }
    
    public static func logoutUser() {
        Session.remove()
        Credentials.remove()
    }
    
    public static func writeCredentials(loginParameters: LoginParameters){
        Credentials.write(loginParameters)
    }
    
    public static func updatePassword(password: String){
        if let userName = Credentials.read()?.userName {
            Credentials.write(LoginParameters(userName: userName, password: password))
        }
    }
    
    struct Session
    {
        private static let SessionKey = "SessionKey"
        
        static func read() -> String? {
            return KeychainWrapper.standardKeychainAccess().stringForKey(SessionKey)
        }
        
        static func write(session: String) {
            KeychainWrapper.standardKeychainAccess().setString(session, forKey: SessionKey)
        }
        
        static func remove() {
            KeychainWrapper.standardKeychainAccess().removeObjectForKey(SessionKey)
        }
    }
    
    struct Credentials
    {
        private static let UserNameKey = "UserNameKey"
        private static let PasswordKey = "PasswordKey"
        
        
        static func read() -> LoginParameters?
        {
            if let userName = KeychainWrapper.standardKeychainAccess().stringForKey(UserNameKey),
                let password = KeychainWrapper.standardKeychainAccess().stringForKey(PasswordKey)
            {
                return LoginParameters(userName: userName, password: password)
            }
            dlog("The keychain lacks a key for email and password. Did you store the credentials after login?",.Warn)
            dlog("    \(UserNameKey): \(KeychainWrapper.standardKeychainAccess().stringForKey(UserNameKey))")
            dlog("    \(PasswordKey): \(KeychainWrapper.standardKeychainAccess().stringForKey(PasswordKey))")
            return nil
        }
        
        static func write(loginParameters: LoginParameters) {
            KeychainWrapper.standardKeychainAccess().setString(loginParameters.userName as! String,forKey: UserNameKey)
            KeychainWrapper.standardKeychainAccess().setString(loginParameters.password as! String,forKey: PasswordKey)
            dlog("Credentials saved: \(loginParameters.userName), \(loginParameters.password)")
        }
        
        static func remove() {
            KeychainWrapper.standardKeychainAccess().removeObjectForKey(UserNameKey)
            KeychainWrapper.standardKeychainAccess().removeObjectForKey(PasswordKey)
            dlog("Removed credentials.")
        }
    }
    
}