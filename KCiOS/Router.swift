
import Foundation
import Alamofire

/** API actions. */
public enum Router: URLRequestConvertible
{
    case Login(p: LoginParameters)
    case Establishments
    case Register(p: RegisterParameters)

    
    private var path: String
        {
        get {
            switch self {
            case .Login         (_): return "login"
            case .Establishments(_): return "establishments"
            case .Register      (_): return "users"
            }
        }
    }
    
    var endpoint: APIEndpoint
        {
        get {
            switch self {
            case .Login    (_) : return APIEndpoint(.POST, .URLEncodedInURL, path, nil,.Public, LoginResponse.self,NSData())
            case .Establishments    (_) : return APIEndpoint(.GET, .URLEncodedInURL, path, nil,.Public, EstablishmentResponse.self,NSData())

            case .Register        (let p) : return APIEndpoint(.POST, .JSON,   path, nil, .Public, RegisterResponse.self, p.registerModelToData())
            }
        }
    }
    
    // MARK:- URLRequestConvertible
    
    public var URLRequest: NSMutableURLRequest
    {
        let url: NSURL = (APIConstants.BaseURL as NSURL).URLByAppendingPathComponent(self.endpoint.path)
        var mutableURLRequest = NSMutableURLRequest(URL: url)
        mutableURLRequest.HTTPMethod = self.endpoint.method.rawValue
        mutableURLRequest = endpoint.encoding.encode(mutableURLRequest, parameters: self.endpoint.parameters).0
        //mutableURLRequest.HTTPBody = endpoint.body
        
        switch self {
        case .Login(_):
            mutableURLRequest.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
            
        case .Establishments(_):
            mutableURLRequest.setValue("application/json", forHTTPHeaderField: "Accept")
            
        case .Register(_):
            mutableURLRequest.setValue("application/json", forHTTPHeaderField: "Content-Type")
        }
        return mutableURLRequest
    }
    
}

