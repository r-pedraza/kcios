//
//  User.swift
//  KCiOS
//
//  Created by Raúl Pedraza on 9/7/16.
//  Copyright © 2016 Raúl Pedraza. All rights reserved.
//

import UIKit

class User: NSObject {
    
    var name:String!
    var firstSurname:String!
    var secondSurname:String?
    var email:String!
    var userName:String!
    var city:String!
    var avatar:UIImage!
    var token:String!
    var latitude:Float?
    var longitude:Float?
    var isUserLoggedInApp:Bool?
    
    init(dictionary:Dictionary<String,AnyObject>) {
        self.name = dictionary["name"] as! String
    }
    
    init(name:String,firstSurname:String,secondSurname:String,userName:String,email:String,avatar:UIImage) {
        self.name          = name;
        self.firstSurname  = firstSurname
        self.secondSurname = secondSurname
        self.userName      = userName
        self.email         = email
        self.userName      = userName
        self.firstSurname  = firstSurname
        self.isUserLoggedInApp = true
    }

}
