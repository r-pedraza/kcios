
import UIKit

class MenuCellModel
{
    let iconNormal: UIImage
    let iconPressed: UIImage
    let title: String
    let controllerId: String
    let state: MenuOptionState
    
    init(iconNormal: UIImage, iconPressed: UIImage, title: String, controllerId: String, state: MenuOptionState)
    {
        self.iconNormal = iconNormal
        self.iconPressed = iconPressed
        self.title = title
        self.controllerId = controllerId
        self.state = state
    }
}
