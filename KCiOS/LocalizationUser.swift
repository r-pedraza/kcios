//
//  LocalizationUser.swift
//  KCiOS
//
//  Created by Raúl Pedraza on 9/7/16.
//  Copyright © 2016 Raúl Pedraza. All rights reserved.
//

import UIKit

class LocalizationUser: NSObject {
    var latitude:Float?
    var longitude:Float?
    var city:String?
}
