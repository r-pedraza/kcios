//
//  ServicesCell.swift
//  KCiOS
//
//  Created by Raúl Pedraza on 29/9/16.
//  Copyright © 2016 Raúl Pedraza. All rights reserved.
//

import UIKit

class ServicesCell: UITableViewCell {

    @IBOutlet weak var serviceTitleLbl: UILabel!
    @IBOutlet weak var priceLbl: UILabel!
    @IBOutlet weak var appointmentBtn: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
 

        // Configure the view for the selected state
    }
    
    @IBAction func aapoinmentAction(sender: AnyObject) {
        
        self.appointmentBtn.titleLabel?.text = "10/11 - 14h"
    }
    
    
}
