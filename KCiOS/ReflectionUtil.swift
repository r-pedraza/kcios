//
//  ReflectionUtil.swift
//  KCiOS
//
//  Created by Raúl Pedraza on 12/7/16.
//  Copyright © 2016 Raúl Pedraza. All rights reserved.
//


import Foundation

public struct ReflectionUtil
{
    public static func variablesOfObject <T: AnyObject>(object: AnyObject, withClass: T) -> [String: T]
    {
        var keyValueDic: Dictionary<String,T> = [:]
        ReflectionUtil.propertyNamesOfObject(object).each { (name: String) in
            if let value = object.valueForKey(name), castedValue = value as? T {
                keyValueDic[name] = castedValue
            }
        }
        return keyValueDic
    }
    
    
    public static func unwrap(any:Any) -> Any
    {
        let mi = Mirror(reflecting: any)
        if mi.displayStyle != .Optional {
            return any
        }
        if mi.children.count == 0 { return NSNull() }
        let (_, some) = mi.children.first!
        return some
        
    }
    
    public static func dictionaryForAny(object: Any) -> Dictionary<String,AnyObject>
    {
        var properties = Dictionary<String,AnyObject>()
        for case let (name, value) in Mirror(reflecting: object).children {
            let isNil = "\(value)" == "nil"
            if !isNil {
                if let name = name {
                    if let p = value as? DictionaryConvertible {
                        properties["\(name)"] = p.dictionary
                    } else {
                        let any = ReflectionUtil.unwrap(value)
                        properties["\(name)"] = "\(any)"
                    }
                    
                }
            }
        }
        return properties
    }
    
    // Returns all non nil properties of the given object as a name:value Dictionary.
    public static func toDictionary(object: AnyObject) -> Dictionary<String,AnyObject>? {
        var properties: Dictionary<String,AnyObject> = [:]
        ReflectionUtil.propertyNamesOfObject(object).each { (name: String) in
            if let value = object.valueForKey(name) {
                properties[name] = value
            }
        }
        return properties
    }
    
    public static func hasNilProperties(any: Any) -> Bool {
        for case let (p, value) in Mirror(reflecting: any).children {
            if "\(value)" == "nil" { // <-- only way I found to check for nil values
                dlog("\(p) is nil")
                return true
            }
        }
        return false
    }
    
    // Retrieves an array of property names found on the current object
    // using Objective-C runtime functions for introspection:
    // https://developer.apple.com/library/mac/documentation/Cocoa/Conceptual/ObjCRuntimeGuide/Articles/ocrtPropertyIntrospection.html
    //
    public static func propertyNamesOfObject(object: AnyObject) -> Array<String> {
        var results: Array<String> = [];
        
        // retrieve the properties via the class_copyPropertyList function
        var count: UInt32 = 0;
        let myClass: AnyClass? = object.classForCoder;
        guard myClass != nil else {
            return ["(description not available for object \(object))"]
        }
        
        let properties = class_copyPropertyList(myClass, &count);
        
        // iterate each objc_property_t struct
        for i: UInt32 in 0 ..< count {
            let property = properties[Int(i)];
            
            // retrieve the property name by calling property_getName function
            let cname = property_getName(property);
            
            // covert the c string into a Swift string
            let name = String.fromCString(cname);
            results.append(name!);
        }
        
        // release objc_property_t structs
        free(properties);
        
        return results;
    }
    
    
    public static func descriptionOfObject(object: AnyObject) -> String
    {
        let diz: String = NSStringFromClass(object_getClass(object))
        let properties = self.propertyNamesOfObject(object)
        let valueProperties: [String] = properties.map {
            let value = object.valueForKey($0)
            return "\n\t\($0) = \(value == nil ? "nil" : value!)"
        }
        return diz + ": " + valueProperties.joinWithSeparator(", ")
    }
    
}