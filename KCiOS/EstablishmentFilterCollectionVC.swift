//
//  EstablishmentFilterCollectionVC.swift
//  KCiOS
//
//  Created by Raúl Pedraza on 8/9/16.
//  Copyright © 2016 Raúl Pedraza. All rights reserved.
//

import UIKit

class EstablishmentFilterCollectionVC: UICollectionViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
          self.collectionView!.registerNib(UINib(nibName: TitleNibCell.EstablishmentFilterNibCell.rawValue, bundle: nil), forCellWithReuseIdentifier: "EstablishmentFilterCell")

    }

    // MARK: UICollectionViewDataSource

    override func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }


    override func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 10
    }

    override func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
       let establishmentCell = collectionView.dequeueReusableCellWithReuseIdentifier("EstablishmentFilterCell" , forIndexPath: indexPath) as? EstablishmentFilterCell
        establishmentCell?.establishmentFilterLabel.text = "kkkkkkk"
        
        return establishmentCell!
    }
    

    // MARK: UICollectionViewDelegate

    /*
    // Uncomment this method to specify if the specified item should be highlighted during tracking
    override func collectionView(collectionView: UICollectionView, shouldHighlightItemAtIndexPath indexPath: NSIndexPath) -> Bool {
        return true
    }
    */

    /*
    // Uncomment this method to specify if the specified item should be selected
    override func collectionView(collectionView: UICollectionView, shouldSelectItemAtIndexPath indexPath: NSIndexPath) -> Bool {
        return true
    }
    */

    /*
    // Uncomment these methods to specify if an action menu should be displayed for the specified item, and react to actions performed on the item
    override func collectionView(collectionView: UICollectionView, shouldShowMenuForItemAtIndexPath indexPath: NSIndexPath) -> Bool {
        return false
    }

    override func collectionView(collectionView: UICollectionView, canPerformAction action: Selector, forItemAtIndexPath indexPath: NSIndexPath, withSender sender: AnyObject?) -> Bool {
        return false
    }

    override func collectionView(collectionView: UICollectionView, performAction action: Selector, forItemAtIndexPath indexPath: NSIndexPath, withSender sender: AnyObject?) {
    
    }
    */

}
