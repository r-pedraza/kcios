//
//  LoginModel.swift
//  KCiOS
//
//  Created by Raúl Pedraza on 19/6/16.
//  Copyright © 2016 Raúl Pedraza. All rights reserved.
//

import UIKit

class LoginModel: NSObject {
    
    var userName:AnyObject!
    var password:AnyObject!
    
    init(userName:AnyObject,password:AnyObject) {
        self.userName = userName;
        self.password = password;
    }
    
    func loginModelToDictionary()->[String : AnyObject]{
        return ["userName":self.userName,"password":self.password]
    }

}
