//
//  EstablishmentCell.swift
//  KCiOS
//
//  Created by Raúl Pedraza on 3/9/16.
//  Copyright © 2016 Raúl Pedraza. All rights reserved.
//

import UIKit

class EstablishmentCell: UITableViewCell {

    @IBOutlet weak var mainEstablishmentImage: UIImageView!
    @IBOutlet weak var distanceLabel: UILabel!
    @IBOutlet weak var commentsLabel: UILabel!
    var expandCell:Bool!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .None
        self.backgroundColor = UIColor(red: 0.12, green: 0.54, blue: 0.78, alpha: 1.00)
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    //MARK : ACTIONS
    @IBAction func addEstablishmentAction(sender: AnyObject) {
    }
}
