//
//  RegisterParameters.swift
//  KCiOS
//
//  Created by Raúl Pedraza on 25/9/16.
//  Copyright © 2016 Raúl Pedraza. All rights reserved.
//

import Foundation

public struct RegisterParameters:DictionaryConvertible {
    
     let userName: AnyObject
     let password: AnyObject
     let email:    AnyObject
     let name:     AnyObject
    
    init(userName: AnyObject, password: AnyObject,email:AnyObject,name:AnyObject){
        self.userName = userName
        self.password = password
        self.email    = email
        self.name     = name
    }
    
  public func registerModelToData()->NSData{
    
   let dic = ["userName":self.userName,
     "password":self.password,
     "email"   :self.email,
     "name"    :self.name]
    
    let jsonData = try! NSJSONSerialization.dataWithJSONObject(dic, options: NSJSONWritingOptions.PrettyPrinted)
    
   return jsonData
    }
    
}