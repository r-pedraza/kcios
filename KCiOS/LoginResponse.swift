//
//  LoginResponse.swift
//  KCiOS
//
//  Created by Raúl Pedraza on 12/7/16.
//  Copyright © 2016 Raúl Pedraza. All rights reserved.
//

import UIKit
import Foundation

/** Response from a login operation. */
public struct LoginResponse: JSONResponse
{
    public var token:String?
    
    public static func createInstance(json: ResponseDictionary) -> LoginResponse?
    {
        var u = LoginResponse()
        u.token = json["response"]!["data"] as? String

        return u
    }
}
