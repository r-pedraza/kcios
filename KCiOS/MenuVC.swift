
import UIKit

enum MenuOptionState {
    case Default
    case Disabled
    case Hidden
}


class MenuVC: UIViewController
{
    
    
    // MARK:- Data creation
    
    private let model:(String,String,String,String,MenuOptionState)->MenuCellModel = { iconNormal, iconPressed, title, controllerId, state in
        return MenuCellModel(iconNormal: UIImage(), iconPressed: UIImage(), title: title, controllerId: controllerId, state: state)
    }
    
    private func createRowsData() -> [MenuCellModel]
    {
        var rows: [MenuCellModel] = [
            model("MyAccount", "MyAccount","My Account","Register",.Default),
            model("Favorites", "Favorites","Favorites", "Favorites", .Default),
                  model("Favorites", "Favorites","Appointments", "Favorites", .Default),
            model("menu-shopsWhite",     "menu-shops","Search" ,  "Favorites",     .Default),
            model("menu-helpWhite",      "menu-help","Logout", "Logout",    .Default)
        ]
        rows = rows.filter { model -> Bool in
            return model.state != .Hidden
        }
        return rows
    }
    
    // MARK:- Initialization
    
    private func menuView() -> MenuView
    {
        if let menuView = self.view as? MenuView {
            return menuView
        } else {
            assert(false, "Expected a MenuView as self.view")
            return MenuView()
        }
    }

    func prepareDataForView(){
        menuView().setRows(createRowsData())
    }
    
    private func prepareView(menuView: MenuView)
    {
        menuView.handleClick = { model in
            
            if let delegate = UIApplication.sharedApplication().delegate as? AppDelegate,
                let navigationController = delegate.window?.rootViewController as? UINavigationController,
                let slider:Slider = delegate.slider
            {
                
                dlog("pushing \(model.controllerId)")
                slider.togglePosition()
                RoutingScreen().routingTo(model.controllerId, controller: model.controllerId, navigation: navigationController)
            }
        }
    }

    
    // MARK:- UIViewController
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        prepareView(menuView())
    }
    
    override func viewWillAppear(animated: Bool)
    {
        prepareDataForView()
        super.viewWillAppear(animated)
    }
}
