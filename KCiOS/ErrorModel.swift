//
//  ErrorModel.swift
//  KCiOS
//
//  Created by Raúl Pedraza on 7/7/16.
//  Copyright © 2016 Raúl Pedraza. All rights reserved.
//

import UIKit

class ErrorModel: NSObject {
    
    var message:String?
    var code   :String?
    
    init(dictionary: [String:String]) {
        self.code    = dictionary["code"]
        self.message = dictionary["message"]
    }
}
