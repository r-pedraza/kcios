//
//  OperationError.swift
//  KCiOS
//
//  Created by Raúl Pedraza on 12/7/16.
//  Copyright © 2016 Raúl Pedraza. All rights reserved.
//


import Foundation

public enum OperationErrorCause
{
    case NotConnectedToTheInternet  // NSURLErrorNotConnectedToInternet
    case HTTPClientError(Int)       // HTTP error
    case HTTPServerError(Int)       // HTTP error
    case MalformedResponse          // Can't parse the response as JSON
    case MissingCredentials         // Tried to login but didn't have login/password stored
    case TimedOut                   // network operation timed out
    case UnexpectedError            // An unexpected error
    case NetworkConnectionLost      // The network connection was lost
    
    /** Error message intended for developers. */
    var description: String
    {
        switch self {
        case NotConnectedToTheInternet: return ""
            
        default: return ""
        }
    }
}

/** Indicates an error */
public struct OperationError: ErrorType
{
    /** Message that we can show in alert dialogs to the final user. */
    public private(set) var userFriendlyMessage: String = ""
    
    /** Caue of this error. */
    public private(set) var cause: OperationErrorCause
    
    /** Human readable indication of where this error was created. */
    public private(set) var origin: String
    
    /** Root cause of this error. */
    public private(set) var underlyingError: ErrorType?
    
    public init(
        userFriendlyMessage: String,
        file: String = #file,
        line: Int = #line,
        cause: OperationErrorCause,
        function: String = #function,
        underlyingError: ErrorType? = nil)
    {
        self.userFriendlyMessage = userFriendlyMessage
        self.cause = cause
        self.origin = "Created in \(function) (File: \(file) Line: \(line))."
        self.underlyingError = underlyingError
    }
    
    public init(_ cause: OperationErrorCause, file: String = #file, line: Int = #line, function: String = #function, underlyingError: ErrorType? = nil){
        self.userFriendlyMessage = cause.description
        self.cause = cause
        self.origin = "Created in \(function) (File: \(file) Line: \(line))."
        self.underlyingError = underlyingError
    }
    
}
