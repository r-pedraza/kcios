//
//  MenuActionProtocol.swift
//  KCiOS
//
//  Created by Raúl Pedraza on 7/9/16.
//  Copyright © 2016 Raúl Pedraza. All rights reserved.
//

import Foundation
import UIKit

public protocol MenuActionProtocol{
    func handleDragGesture(recognizer: UIPanGestureRecognizer)

}