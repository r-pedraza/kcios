//
//  RegisterTableViewController.swift
//  KCiOS
//
//  Created by Raúl Pedraza on 9/7/16.
//  Copyright © 2016 Raúl Pedraza. All rights reserved.
//

import UIKit


class RegisterTableViewController: UITableViewController{
    
    final let formVM = SignupViewModel()
    
    override func viewDidLoad() {
        tableView!.registerNib(UINib(nibName: TitleNibCell.RegisterNibCell.rawValue, bundle: nil), forCellReuseIdentifier: RegisterFormCell().identifierCell())
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: UIImage(named: "MyAccount"), style: UIBarButtonItemStyle.Plain, target: self, action: #selector(registerAction))
    }
    
    // MARK: - Table view data source
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.formVM.formFields.count
    }
    
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {        
        return  FactoryCell().cellWithObject(formVM.formFields[indexPath.row], tableView: tableView) as! RegisterFormCell
    }
    
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return RegisterFormCell().cellHeigth()
    }
    
    
    func registerAction() {
    
    }
    
}
