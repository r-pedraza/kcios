
import UIKit

// MARK:- ConfigurableCell

protocol ConfigurableCell {
    static var reuseIdentifier: String { get }
    static var nib: UINib? { get }
    func configureWithObject<T>(object: T) -> Void
}

extension ConfigurableCell {
    static var reuseIdentifier: String { return String(Self) }
    static var nib: UINib? { return nil }
}


// MARK:- UITableView

extension UITableView {
    
    func registerReusableCell<T: UITableViewCell where T: ConfigurableCell>(_: T.Type) {
        if let nib = T.nib {
            self.registerNib(nib, forCellReuseIdentifier: T.reuseIdentifier)
        } else {
            self.registerClass(T.self, forCellReuseIdentifier: T.reuseIdentifier)
        }
    }
}


// MARK:- TableDataSource

class TableDataSource: NSObject
{
    private let data: Array<AnyObject>
    private weak var tableView: UITableView?
    private let reuseIdentifier: String
    
    required init <T where T: UITableViewCell, T:ConfigurableCell> (data: Array<AnyObject>, tableView: UITableView, cell: T.Type)
    {
        self.reuseIdentifier = T.reuseIdentifier
        self.tableView = tableView
        self.data = data
        super.init()
        
        self.tableView!.registerReusableCell(cell.self)
    }
}


extension TableDataSource: UITableViewDataSource
{
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        let cell: AnyObject? = tableView.dequeueReusableCellWithIdentifier(reuseIdentifier, forIndexPath: indexPath)
        let object = data[indexPath.row]
        (cell as! ConfigurableCell).configureWithObject(object)
        
        return (cell as! UITableViewCell)
    }
    
}

