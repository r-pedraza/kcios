//
//  LoginParameters.swift
//  KCiOS
//
//  Created by Raúl Pedraza on 11/7/16.
//  Copyright © 2016 Raúl Pedraza. All rights reserved.
//


import Foundation

/** Parameters for a login. */
public struct LoginParameters: DictionaryConvertible
{
    public let userName: AnyObject
    public let password: AnyObject
    
    public init(userName: AnyObject, password: AnyObject){
        self.userName = userName
        self.password = password
    }
    
    func loginModelToData()->NSData{
        let dic = ["userName":self.userName,"password":self.password]
 
        let jsonData = try! NSJSONSerialization.dataWithJSONObject(dic, options: NSJSONWritingOptions.PrettyPrinted)
        
        return jsonData
    }
}