
//
//  EstablishmentsTableViewController.swift
//  KCiOS
//
//  Created by Raúl Pedraza on 6/9/16.
//  Copyright © 2016 Raúl Pedraza. All rights reserved.
//

import UIKit

class EstablishmentsTableVC: UITableViewController,UISearchBarDelegate {
    
    var images = Array<UIImage>()
    var distances = Array<String>()
    var establishment = Array<Establishment>()
    var delegate:MenuActionProtocol!
    let searchController = UISearchController(searchResultsController: nil)
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.separatorStyle = .None
        tableView.backgroundColor = UIColor(red: 0.12, green: 0.54, blue: 0.78, alpha: 1.00)
        tableView!.registerNib(UINib(nibName: TitleNibCell.EstablishmentNibCell.rawValue, bundle: nil), forCellReuseIdentifier: "EstablishmentCell")
        OperationFactory().establishmentsOperation().onSuccess { response in
            if let array = response as? EstablishmentResponse {
                self.createImages()
                self.createDistance()
                self.establishment = array.establishment
                self.tableView!.reloadData()
            }
        }
    }
    
    
    func createImages() {
        let imagePodofis = UIImage(named: "podofis")
        let imageRuiz    = UIImage(named: "ruiz")
        let imageZone    = UIImage(named: "other")
        images.append(imagePodofis!)
        images.append(imageRuiz!)
        images.append(imageZone!)
    }
    
    func createDistance() {
        let podofis = "500 m"
        let ruiz    = "1.5 km"
        let other   = "10 km"
        distances.append(podofis)
        distances.append(ruiz)
        distances.append(other)
        
    }
    // MARK: - Table view data source
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.establishment.count
    }
    
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let establishment       = self.establishment[indexPath.row]
        establishment.imageMain = images[indexPath.row]
        establishment.distance  = distances[indexPath.row]
        return FactoryCell().cellWithObject(self.establishment[indexPath.row], tableView: tableView) as! EstablishmentCell
    }
    
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 200
    }
    
    //MARK: -  Table view delegate
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        RoutingScreen().routingToEstablishmentDetail(self.establishment[indexPath.row], navigation: self.navigationController!)
    }
    
    override func tableView(tableView: UITableView, editActionsForRowAtIndexPath indexPath: NSIndexPath) -> [UITableViewRowAction]? {
        
        let location = UITableViewRowAction(style: .Normal, title: "Location") { action, index in
            RoutingScreen().routingToEstablishmentDetailWithMapView(self.establishment[index.row], navigation: self.navigationController!)
            
        }
        location.backgroundColor = UIColor(red: 0.13, green: 0.32, blue: 0.55, alpha: 0.5)
        let favorite = UITableViewRowAction(style: .Normal, title: "Favorite") { action, index in
            RoutingScreen().routingTo("Favorites", controller: "Favorites", navigation: self.navigationController!)
        }
        favorite.backgroundColor = UIColor(red: 0.13, green: 0.32, blue: 0.55, alpha: 0.8)
        let reserve = UITableViewRowAction(style: .Normal, title: "Reserve") { action, index in
       
               RoutingScreen().routingToEstablishmentDetail(self.establishment[index.row], navigation: self.navigationController!)
        }
        reserve.backgroundColor = UIColor(red: 0.13, green: 0.32, blue: 0.55, alpha: 1.0)
        return [reserve, favorite, location]
    }
    
    
    //MARK: - Actions
    
    @IBAction func menuAction(sender: UIBarButtonItem) {
        (UIApplication.sharedApplication().delegate as? AppDelegate)?.slider.togglePosition()
    }

    @IBAction func searchAction(sender: AnyObject) {
     
        // Set any properties (in this case, don't hide the nav bar and don't show the emoji keyboard option)
        searchController.hidesNavigationBarDuringPresentation = false
        searchController.searchBar.barTintColor = UIColor(red: 0.13, green: 0.32, blue: 0.55, alpha: 1.00)
        searchController.searchBar.keyboardType = UIKeyboardType.ASCIICapable
       let cancelBtn = (UIBarButtonItem.appearanceWhenContainedInInstancesOfClasses([UISearchBar.self]))
         cancelBtn.tintColor = UIColor.whiteColor()
         cancelBtn.title = "Done"
        // Make this class the delegate and present the search
        searchController.searchBar.delegate = self
        presentViewController(searchController, animated: true, completion: nil)
    }
    
    @IBAction func myAccountAction(sender: AnyObject) {
        
        let alertController = UIAlertController(title: "Register or Login", message: "You need to be registered to access certain features of the app", preferredStyle: UIAlertControllerStyle.Alert)
        let DestructiveAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.Destructive) { (result : UIAlertAction) -> Void in
            alertController.dismissViewControllerAnimated(true, completion: nil)
        }
        let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default) { (result : UIAlertAction) -> Void in
            RoutingScreen().routingToLogin(self.navigationController!)
        }
        alertController.addAction(DestructiveAction)
        alertController.addAction(okAction)
        self.presentViewController(alertController, animated: true, completion: nil)
  
    }
    
    
}
