//
//  ProductForm.swift
//  KCiOS
//
//  Created by Raúl Pedraza on 15/7/16.
//  Copyright © 2016 Raúl Pedraza. All rights reserved.
//

import UIKit

enum TitleProductForm:String {
    case Name        = "Name"
    case Description = "Description"
    case Category    = "Category"
    case State       = "State"
    case Price       = "Price"
    case Seller      = "Seller"
    case Image       = "Image"
}

class ProductFormField {
    var placeholder:String?
    var value:String?
    
    init(placeholder:String){
        self.placeholder = placeholder
    }
}

class ProductViewModel{
    
    var productFormFields = [ProductFormField]()
    
    init() {
        setupFormFields()
    }
    
   private func setupFormFields() {
        // Setup the fields of the signup form
        
        let name        = ProductFormField(placeholder:titles(0))
        let description = ProductFormField(placeholder:titles(1))
        let category    = ProductFormField(placeholder:titles(2))
        let state       = ProductFormField(placeholder:titles(3))
        let price       = ProductFormField(placeholder:titles(4))
        let seller      = ProductFormField(placeholder:titles(5))
        let image       = ProductFormField(placeholder:titles(6))

        self.productFormFields = [name,description,category,state,price,seller,image]
    }
    
    func titles(position:Int) -> String {
        var titles = [TitleProductForm.Name.rawValue,
                      TitleProductForm.Description.rawValue,
                      TitleProductForm.Category.rawValue,
                      TitleProductForm.State.rawValue,
                      TitleProductForm.Price.rawValue,
                      TitleProductForm.Seller.rawValue,
                      TitleProductForm.Image.rawValue];
        
        return titles[position]
        
    }
}