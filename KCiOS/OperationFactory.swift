//
//  OperationFactory.swift
//  KCiOS
//
//  Created by Raúl Pedraza on 12/7/16.
//  Copyright © 2016 Raúl Pedraza. All rights reserved.
//


import Foundation
import Alamofire

public protocol OperationFactoryType
{
    func loginOperation(parameters: LoginParameters) -> Operation
    func establishmentsOperation() -> Operation
    func registerUserOperation(parameters:RegisterParameters) -> Operation
}


/** Creates operations. */
public struct OperationFactory: OperationFactoryType
{
    private let manager: Alamofire.Manager
    
    /** Initializer. It creates a private Alamofire.Manager. */
    public init()
    {
        
        let defaultHeaders                      = Alamofire.Manager.sharedInstance.session.configuration.HTTPAdditionalHeaders ?? [:]
        let configuration                       = NSURLSessionConfiguration.defaultSessionConfiguration()
        configuration.HTTPAdditionalHeaders     = defaultHeaders
        configuration.timeoutIntervalForRequest = APIConstants.TimeoutIntervalForRequest
        manager                                 = Manager(configuration: configuration)
        
    }

    /** Returns an operation able to perform a login. */
    public func loginOperation(parameters: LoginParameters) -> Operation {
        return Operation(manager: manager, router: Router.Login(p: parameters))
    }
    
    /** Returns an operation able to perform a establishments. */
    public func establishmentsOperation() -> Operation {
        return Operation(manager: manager, router: Router.Establishments)
    }
    /** Returns an operation able to perform a register user. */
    public func registerUserOperation(parameters:RegisterParameters) -> Operation {
        return Operation(manager: manager, router: Router.Register(p: parameters))
    }
    
}
