//
//  FormField.swift
//  KCiOS
//
//  Created by Raúl Pedraza on 10/7/16.
//  Copyright © 2016 Raúl Pedraza. All rights reserved.
//

import UIKit

enum TitleForm:String {
    case Name            = "Name"
    case FirstSurName    = "First Surname"
    case SecondSurName   = "Second Surname"
    case Email           = "Email"
    case UserName        = "User Name"
    case Password        = "Password"
    case ConfirmPassword = "Confirm Password"
    case City            = "City"
    case State           = "State"
    case Avatar          = "Avatar"
}

class FormField {
    
    var placeholder:String?
    var value:String?
    var tag:Int?
    
    init(placeholder:String,tag:Int){
        self.placeholder = placeholder
        self.tag         = tag
    }
}

class SignupViewModel {
    
    var formFields = [FormField]()
    
    init() {
        setupFormFields()
    }
    
    func setupFormFields() {
        // Setup the fields of the signup form
        
        let name            = FormField(placeholder:placeholder(0),tag:0)
        let firstSurname    = FormField(placeholder:placeholder(1),tag:1)
        let secondSurname   = FormField(placeholder:placeholder(2),tag:2)
        let email           = FormField(placeholder:placeholder(3),tag:3)
        let userName        = FormField(placeholder:placeholder(4),tag:4)
        let password        = FormField(placeholder:placeholder(5),tag:5)
        let confirmPassword = FormField(placeholder:placeholder(6),tag:6)
        let city            = FormField(placeholder:placeholder(7),tag:7)
        let state           = FormField(placeholder:placeholder(8),tag:8)

        self.formFields = [name,firstSurname,secondSurname,email,userName,password,confirmPassword,city,state]
    }
    
    func placeholder(position:Int) -> String {
        var titles = [TitleForm.Name.rawValue,
                      TitleForm.FirstSurName.rawValue,
                      TitleForm.SecondSurName.rawValue,
                      TitleForm.Email.rawValue,
                      TitleForm.UserName.rawValue,
                      TitleForm.Password.rawValue,
                      TitleForm.ConfirmPassword.rawValue,
                      TitleForm.City.rawValue,
                      TitleForm.State.rawValue,
                      TitleForm.Avatar.rawValue];
        
        return titles[position]

    }
}