
import Foundation

let defaultLogger = SimpleLogger()

/**
Shortcut global function to call a default logger.
Because life is too short to configure logging in utility classes.

Usage:
    dlog("kitchen is burning")
    dlog("kitchen is burning", .Warning)
*/
public func dlog<T>(
    message: T,
    _ logLevel: LogLevel = .Debug,
    _ classOrigin: Any? = #file ,
    _ functionOrigin: String = #function,
    _ line: UInt = #line)
{
    defaultLogger.log(message, logLevel, classOrigin, functionOrigin, line)
}
