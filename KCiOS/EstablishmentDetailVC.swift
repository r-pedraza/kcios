//
//  EstablishmentDetailVC.swift
//  KCiOS
//
//  Created by Raúl Pedraza on 7/9/16.
//  Copyright © 2016 Raúl Pedraza. All rights reserved.
//

import UIKit
import MapKit
import CVCalendar

class EstablishmentDetailVC: UIViewController,CVCalendarViewDelegate,CVCalendarMenuViewDelegate{
    @IBOutlet weak var scroolVIew: UIScrollView!
    @IBOutlet weak var calendarButton: UIButton!
    let locationManager = CLLocationManager()
    @IBOutlet weak var establishmentTopImage: UIImageView!
                   var establishment        : Establishment!
    @IBOutlet weak var listServicesTable: UITableView!
    @IBOutlet weak var bottomImg: NSLayoutConstraint!
    @IBOutlet weak var map: MKMapView!
    @IBOutlet weak var calendarView: CVCalendarView!
    
    var shouldShowDaysOut = true
    var animationFinished = true
    var selectedDay:DayView!
    var tag:Int?
    
    //MARK: Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
        self.title = establishment.name
        calendarView.delegate = self
        calendarView.animatorDelegate = self
        calendarView.calendarAppearanceDelegate = self
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        calendarSetup()
    }
    
    //MARK: Actions

    @IBAction func didTapInCalendarAction(sender: AnyObject) {
        configureWichViewToShow(sender.tag)
    }
    @IBAction func didTapInListServicesAction(sender: AnyObject) {
        configureWichViewToShow(sender.tag)
    }
    @IBAction func didTapInMapAction(sender: AnyObject) {
        configureWichViewToShow(sender.tag)
    }
    
    
    //MARK: Private Methods
    
    func setup() {
       viewSetup()
    }
    
    
    func mapKitSetup() {
        
        self.locationManager.requestAlwaysAuthorization()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestWhenInUseAuthorization()
        locationManager.requestLocation()
        map.showsUserLocation = true
        if let latitude = Double(establishment.latitude!), let longitude = Double(establishment.longitude!) {
            establishmentPin(latitude, longitude:longitude)
            centerMapOnLocation(CLLocation(latitude:latitude, longitude: longitude))
        }
    }
    
    func calendarSetup() {
        calendarView.commitCalendarViewUpdate()
    }
    
    func viewSetup()  {
        navigationController!.navigationBar.tintColor = UIColor.whiteColor();
        navigationController!.navigationBar.topItem?.title = establishment.name;

        scroolVIew.backgroundColor = UIColor(red: 0.12, green: 0.54, blue: 0.78, alpha: 1.00)
        listServicesTable.backgroundColor = UIColor(red: 0.12, green: 0.54, blue: 0.78, alpha: 0.5)
        self.listServicesTable.allowsMultipleSelection = true
        listServicesTable.registerNib(UINib.init(nibName: "ServicesCell", bundle: nil), forCellReuseIdentifier: "servicesCell")
        mapKitSetup()
        if tag == 2 {
            configureWichViewToShow(tag!)
        }else{
            configureWichViewToShow(3)
        }
    }
    
    func configureWichViewToShow(tag:Int) {
        
        switch tag {
        case 0:
            self.calendarView.hidden      = false
            self.map.hidden               = true
            self.listServicesTable.hidden = true

            break
        case 1:
            self.listServicesTable.hidden = false
            self.calendarView.hidden      = true
            self.map.hidden               = true
            break
        case 2:
            self.map.hidden               = false
            self.calendarView.hidden      = true
            self.listServicesTable.hidden = true
            break
        default:
            self.calendarView.hidden      = true
            self.map.hidden               = true
            self.listServicesTable.hidden = false
            break
        }
    }
    
    let regionRadius: CLLocationDistance = 1000
    func centerMapOnLocation(location: CLLocation) {
        let coordinateRegion = MKCoordinateRegionMakeWithDistance(location.coordinate,
                                                                  regionRadius * 2.0, regionRadius * 2.0)
        map.setRegion(coordinateRegion, animated: true)
    }
}

extension EstablishmentDetailVC : CLLocationManagerDelegate,MKMapViewDelegate {

    func locationManager(manager: CLLocationManager, didChangeAuthorizationStatus status: CLAuthorizationStatus) {
        if status == .AuthorizedWhenInUse {
            locationManager.requestLocation()
        }
    }
    
    func locationManager(manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        let center = CLLocationCoordinate2D(latitude: Double(establishment.latitude!)!, longitude: Double(establishment.longitude!)!)
        let region = MKCoordinateRegion(center: center, span: MKCoordinateSpan(latitudeDelta: 0.01, longitudeDelta: 0.01))
        map.setRegion(region, animated: true)
    }
    
    func mapView(mapView: MKMapView, didUpdateUserLocation userLocation: MKUserLocation) {
//        let region = MKCoordinateRegionMakeWithDistance(userLocation.coordinate, 800, 800)
//        map.setRegion(map.regionThatFits(region), animated: true)
   
    }
    
    func locationManager(manager: CLLocationManager, didFailWithError error: NSError) {
        print("error:: (error)")
    }
    
    func establishmentPin(latitude:Double,longitude:Double)  {
        let anotation = MKPointAnnotation()
        let location = CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
        anotation.coordinate = location
        anotation.title    = establishment.name
        anotation.subtitle = establishment.address
        map.addAnnotation(anotation)

    }
}

extension EstablishmentDetailVC{

    /// Required method to implement!
    func presentationMode() -> CalendarMode {
        return .MonthView
    }
    
    /// Required method to implement!
    func firstWeekday() -> Weekday {
        return .Sunday
    }
    
    // MARK: Optional methods
    
    func dayOfWeekTextColor(by weekday: Weekday) -> UIColor {
        return weekday == .Sunday ? UIColor(red: 0.13, green: 0.32, blue: 0.55, alpha: 1.00) : UIColor.blackColor()
    }
    
    func shouldShowWeekdaysOut() -> Bool {
        return shouldShowDaysOut
    }
    
    func shouldAnimateResizing() -> Bool {
        return true // Default value is true
    }
    
    private func shouldSelectDayView(dayView: DayView) -> Bool {
        return arc4random_uniform(3) == 0 ? true : false
    }
    
    func didSelectDayView(dayView: CVCalendarDayView, animationDidFinish: Bool) {
        print("\(dayView.date.commonDescription) is selected!")
        selectedDay = dayView
    }
    
}
extension EstablishmentDetailVC:UITableViewDataSource,UITableViewDelegate{

    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return 10
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("servicesCell") as! ServicesCell
        
        cell.serviceTitleLbl.text = "Corte de pelo"
        cell.priceLbl.text = "16€"
        
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        self.calendarButton.hidden = false
    }
    
    func tableView(tableView: UITableView, didDeselectRowAtIndexPath indexPath: NSIndexPath) {
        self.calendarButton.hidden = true

    }
    
    func loadMapView()  {
        configureWichViewToShow(2)
    }
}
