//
//  ProductView.swift
//  KCiOS
//
//  Created by Raúl Pedraza on 15/7/16.
//  Copyright © 2016 Raúl Pedraza. All rights reserved.
//

import UIKit

class ProductView: UICollectionView {
    
    override init(frame: CGRect, collectionViewLayout layout: UICollectionViewLayout) {
        super.init(frame: frame, collectionViewLayout: layout)
        self.collectionViewLayout = layout
        self.frame = frame
        self.setupCollectionView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupCollectionView() {
        let layout = self.collectionViewLayout as! UICollectionViewFlowLayout
        layout.sectionInset = UIEdgeInsetsMake(5, 5, 5, 5);
        layout.minimumInteritemSpacing = 5; // this number could be anything <=5. Need it here because the default is 10.
        layout.itemSize = CGSizeMake((self.frame.size.width - 20)/3, 100)
    }

}
