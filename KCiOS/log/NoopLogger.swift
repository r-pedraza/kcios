
import Foundation


public struct NoopLogger: LoggerType
{
    public func log<T>(message: T, _ logLevel: LogLevel, _ classOrigin: Any?, _ functionOrigin: String, _ line: UInt)
    {
        // no op
    }
}