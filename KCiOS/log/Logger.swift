
import Foundation


/*
To get file.function:line information in the log message, you have to either

- use the #file, #function, #line parameters in the call, which is extremly verbose,
- or call to a function with default #file, #function, #line values in the parameters.

So ideally, we would write a protocol function with default values. However, Swift doesn't
allow defaults in protocol functions.

One way to solve this and still have a switchable logger implementation is to

- write a LoggerType protocol with a log function that lacks default parameters
- write a Logger class with a log function that uses default values,
- initialize Logger with an instance of LoggerType and redirect the logs from Logger.log to LoggerType.log

*/

public enum LogLevel: Int, CustomStringConvertible
{
    case Trace
    case Debug
    case Info
    case Warn
    case Error
    
    /** Human readable indicator of this log level. */
    public var description: String {
        get {
            switch(self){
                case Trace: return ""
                case Debug: return ""
                case Info:  return "✅"
                case Warn : return "⚠️"
                case Error: return "🚫"
            }
        }
    }
}


public protocol LoggerType
{
    // jano: is it possible to autoclosure the message so the string is not built?
    func log<T>(message: T, _ logLevel: LogLevel, _ classOrigin: Any?, _ functionOrigin: String, _ line: UInt)
}


