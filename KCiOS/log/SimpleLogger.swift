
import Foundation


public struct SimpleLogger: LoggerType
{
    public init(){}
    
    public func log<T>(message: T, _ logLevel: LogLevel, _ classOrigin: Any?, _ functionOrigin: String, _ line: UInt)
    {
        print("\(logLevel.description)[\(self.dynamicType.originOf(classOrigin)).\(functionOrigin):\(line)] \(message)")
    }
    
    /** Returns the filename. 
        ie: originOf(#file) returns "SimpleLogger"
     */
    private static func originOf(classOrigin: Any?) -> String
    {
        var origin: String?
        if let classOrigin = classOrigin as? String {
            origin = NSURL(fileURLWithPath: classOrigin).URLByDeletingPathExtension!.lastPathComponent
        } else if let any = classOrigin {
            if any is AnyObject {
                let className = NSStringFromClass(object_getClass(any as! AnyObject))
                origin = "\(className)".componentsSeparatedByString(".").last
            } else {
                origin = "\(any)".componentsSeparatedByString(".").last
            }
        }
        // I don't know of any case where origin is nil, but just in case...
        return (origin == nil) ? "nil" : origin!
    }

}