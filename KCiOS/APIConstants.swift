
import Foundation

struct APIConstants
{
    static let BaseURL: NSURL = NSURL(string: "http://ec2-52-39-107-12.us-west-2.compute.amazonaws.com:3000/apiv2/")!
    static let TimeoutIntervalForRequest = 20.0
}
