//
//  DictionaryConvertible.swift
//  KCiOS
//
//  Created by Raúl Pedraza on 11/7/16.
//  Copyright © 2016 Raúl Pedraza. All rights reserved.
//


import Foundation

public protocol DictionaryConvertible: Any {
    var dictionary: Dictionary<String,AnyObject> { get }
}

public extension DictionaryConvertible
{
    // Return a dictionary of non nil properties for the given object.
    private static func dictionaryFor(object: Any) -> Dictionary<String,AnyObject> {
        var properties = Dictionary<String,AnyObject>()
        for case let (name, value) in Mirror(reflecting: object).children {
            let isNil = "\(value)" == "nil"
            if !isNil {
                if let name = name {
                    if let p = value as? DictionaryConvertible {
                        properties["\(name)"] = p.dictionary
                    } else {
                        properties["\(name)"] = "\(value)"
                    }
                    
                }
            }
        }
        return properties
    }
    
    var dictionary: Dictionary<String,AnyObject> {
        return self.dynamicType.dictionaryFor(self)
    }
}